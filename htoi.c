#include <stdio.h>

int htoi(char*);
int check(char);
int letterCheck(char);
int hexExp(int, int);

int main() {
   char hex[] = "0223aF"; 
   char hex1[] = "0x0223aF";

   printf("%s\t%s\n", hex, hex1);
   printf("%d\t%d\n", htoi(hex), htoi(hex1));

   return 0;
}

int htoi(char *string){
    int i, sum, length;
    i = 0;
    sum = 0;
    length = sizeof(string) / sizeof(string[0]);

    if(string[1] == 'x' || string[1] == 'X'){
        i = 2;
        length = length + 2;
    }

    while(i < length){
        sum = sum + (check(string[i]) * (hexExp(16, length - i - 3)));
        ++i;
    }

    return sum;
}

int check(char character){
    switch(character){
        case '0':
            return 0;
            break;
        case '1':
            return 1;
            break;
        case '2':
            return 2;
            break;
        case '3':
            return 3;
            break;
        case '4':
            return 4;
            break;
        case '5':
            return 5;
            break;
        case '6':
            return 6;
            break;
        case '7':
            return 7;
            break;
        case '8':
            return 8;
            break;
        case '9':
            return 9;
            break;
        default:
            return letterCheck(character);
    }
}

int letterCheck(char character){
   if(character == 'a' || character == 'A')
       return 10;
   else if(character == 'b' || character == 'B')
       return 11;
   else if(character == 'c' || character == 'C')
       return 12;
   else if(character == 'd' || character == 'D')
       return 13;
   else if(character == 'e' || character == 'E')
       return 14;
   else if(character == 'f' || character == 'F')
       return 15;
}

int hexExp(int base, int exponent){
    if(exponent == 0){
        return 1;
    } 
    else if(exponent < 0){
        return 0;
    }
    else {
        return base * hexExp(base, exponent - 1);
    }
}
